import canvas from './Canvas';

export default class Obstacle {
    constructor(x, y, height) {
        this.x = x;
        this.y = y;
        this.width = 50;
        this.height = height;
        this.speed = 1;
    }

    move() {
        this.x -= 2;
    }

    checkCollision(playerX, playerY, playerWidth, playerHeight) {
        if (playerX + playerWidth > this.x && playerX < this.x + this.width && playerY + playerHeight > this.y && playerY < this.y + this.height) {
            console.log("yes");
            return true;
        }

        return false;
    }
}