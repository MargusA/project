import Player from './Player.js';
import Obstacle from './Obstacle.js';
import canvas from './Canvas.js';

export default class Game {
    constructor() {
        this.player = new Player();
        this.obstacles = [];
        this.time = 0;
        this.keys = [];
        this.gameOver = false;
        this.WIDTH = canvas.getCanvas().width;
        this.HEIGHT = canvas.getCanvas().height;

        window.addEventListener('keydown', (e) => this.handleKeyDown(e));
        window.addEventListener('keyup', (e) => this.handleKeyUp(e));
        requestAnimationFrame(time => this.update(time));
    }

    update(time) {
        if (this.gameOver) {
            return;
        }

        const ctx = canvas.getContext2D();
        ctx.clearRect(0, 0, canvas.getCanvas().width, canvas.getCanvas().height);

        if (this.time < 50) {
            this.time += Math.random();
        } else {
            const firstHeight = Math.random() * (this.HEIGHT - this.player.height * 2);
            this.obstacles.push(new Obstacle(this.WIDTH, 0, firstHeight));
            const secondHeight = this.HEIGHT - firstHeight - this.player.height * 2;
            this.obstacles.push(new Obstacle(this.WIDTH, this.HEIGHT - secondHeight, secondHeight));
            this.time = 0;
        }

        for (var i = 0; i < this.obstacles.length; i++) {
            if (this.obstacles[i].checkCollision(this.player.x, this.player.y, this.player.width, this.player.height)) {
                this.gameOver = true;
                return;
            };
            this.obstacles[i].move();
            if (this.obstacles[i].x < 0) {
                this.obstacles.splice(i, 1);
            }
            ctx.fillRect(this.obstacles[i].x, this.obstacles[i].y, this.obstacles[i].width, this.obstacles[i].height);
        }

        if (this.keys[38]) { // Up
            this.player.direction = -1;
            this.player.speed = 4;
        } else if (this.keys[40]) { // Down
            this.player.direction = 1;
            this.player.speed = 4;
        } else {
            this.player.speed = 0;
        }
        if (this.keys[32]) { // Spacebar
            this.player.speed *= 5;
        }

        this.player.move();
        ctx.fillRect(this.player.x, this.player.y, this.player.width, this.player.height);
        requestAnimationFrame(time => this.update(time));
    }

    handleKeyDown(e) {
        this.keys[e.keyCode] = true;
    }

    handleKeyUp(e) {
        this.keys[e.keyCode] = false;
    } 
}