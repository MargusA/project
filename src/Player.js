import canvas from './Canvas.js';

export default class Player {
    constructor() {
        this.x = 5;
        this.y = 5;

        this.width = 60;
        this.direction = 1;
        this.speed = 0;
        this.height = 60;
    }

    move() {
        if (this.inBounds(this.y, canvas.getCanvas().height)) this.y += this.direction * this.speed;
    }

    inBounds(y, maxHeight) {
        if (y + this.speed * this.direction < 0) {
            return false;
        } else if (y + this.speed * this.direction + this.height > maxHeight) {
            return false;
        }

        return true;
    }
}