export default {
    internalData() {
      if (!this.internalDataIsSet) {
          this.canvas = document.querySelector("#game-canvas");
          this.canvas.width = 800;
          this.canvas.height = 600;
          this.context2d = this.canvas.getContext('2d');
          this.internalDataIsSet = true;
      }

      return this;
    },
    getCanvas() {
        return this.internalData().canvas;
    },
    getContext2D() {
        return this.internalData().context2d;
    }
}
