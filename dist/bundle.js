/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    internalData: function internalData() {
        if (!this.internalDataIsSet) {
            this.canvas = document.querySelector("#game-canvas");
            this.canvas.width = 800;
            this.canvas.height = 600;
            this.context2d = this.canvas.getContext('2d');
            this.internalDataIsSet = true;
        }

        return this;
    },
    getCanvas: function getCanvas() {
        return this.internalData().canvas;
    },
    getContext2D: function getContext2D() {
        return this.internalData().context2d;
    }
});

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Player_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Obstacle_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Canvas_js__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var Game = function () {
    function Game() {
        var _this = this;

        _classCallCheck(this, Game);

        this.player = new __WEBPACK_IMPORTED_MODULE_0__Player_js__["a" /* default */]();
        this.obstacles = [];
        this.time = 0;
        this.keys = [];
        this.gameOver = false;
        this.WIDTH = __WEBPACK_IMPORTED_MODULE_2__Canvas_js__["a" /* default */].getCanvas().width;
        this.HEIGHT = __WEBPACK_IMPORTED_MODULE_2__Canvas_js__["a" /* default */].getCanvas().height;

        window.addEventListener('keydown', function (e) {
            return _this.handleKeyDown(e);
        });
        window.addEventListener('keyup', function (e) {
            return _this.handleKeyUp(e);
        });
        requestAnimationFrame(function (time) {
            return _this.update(time);
        });
    }

    _createClass(Game, [{
        key: 'update',
        value: function update(time) {
            var _this2 = this;

            if (this.gameOver) {
                return;
            }

            var ctx = __WEBPACK_IMPORTED_MODULE_2__Canvas_js__["a" /* default */].getContext2D();
            ctx.clearRect(0, 0, __WEBPACK_IMPORTED_MODULE_2__Canvas_js__["a" /* default */].getCanvas().width, __WEBPACK_IMPORTED_MODULE_2__Canvas_js__["a" /* default */].getCanvas().height);

            if (this.time < 50) {
                this.time += Math.random();
            } else {
                var firstHeight = Math.random() * (this.HEIGHT - this.player.height * 2);
                this.obstacles.push(new __WEBPACK_IMPORTED_MODULE_1__Obstacle_js__["a" /* default */](this.WIDTH, 0, firstHeight));
                var secondHeight = this.HEIGHT - firstHeight - this.player.height * 2;
                this.obstacles.push(new __WEBPACK_IMPORTED_MODULE_1__Obstacle_js__["a" /* default */](this.WIDTH, this.HEIGHT - secondHeight, secondHeight));
                this.time = 0;
            }

            for (var i = 0; i < this.obstacles.length; i++) {
                if (this.obstacles[i].checkCollision(this.player.x, this.player.y, this.player.width, this.player.height)) {
                    this.gameOver = true;
                    return;
                };
                this.obstacles[i].move();
                if (this.obstacles[i].x < 0) {
                    this.obstacles.splice(i, 1);
                }
                ctx.fillRect(this.obstacles[i].x, this.obstacles[i].y, this.obstacles[i].width, this.obstacles[i].height);
            }

            if (this.keys[38]) {
                // Up
                this.player.direction = -1;
                this.player.speed = 4;
            } else if (this.keys[40]) {
                // Down
                this.player.direction = 1;
                this.player.speed = 4;
            } else {
                this.player.speed = 0;
            }
            if (this.keys[32]) {
                // Spacebar
                this.player.speed *= 5;
            }

            this.player.move();
            ctx.fillRect(this.player.x, this.player.y, this.player.width, this.player.height);
            requestAnimationFrame(function (time) {
                return _this2.update(time);
            });
        }
    }, {
        key: 'handleKeyDown',
        value: function handleKeyDown(e) {
            this.keys[e.keyCode] = true;
        }
    }, {
        key: 'handleKeyUp',
        value: function handleKeyUp(e) {
            this.keys[e.keyCode] = false;
        }
    }]);

    return Game;
}();

/* harmony default export */ __webpack_exports__["a"] = (Game);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Canvas__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Obstacle = function () {
    function Obstacle(x, y, height) {
        _classCallCheck(this, Obstacle);

        this.x = x;
        this.y = y;
        this.width = 50;
        this.height = height;
        this.speed = 1;
    }

    _createClass(Obstacle, [{
        key: "move",
        value: function move() {
            this.x -= 2;
        }
    }, {
        key: "checkCollision",
        value: function checkCollision(playerX, playerY, playerWidth, playerHeight) {
            if (playerX + playerWidth > this.x && playerX < this.x + this.width && playerY + playerHeight > this.y && playerY < this.y + this.height) {
                console.log("yes");
                return true;
            }

            return false;
        }
    }]);

    return Obstacle;
}();

/* harmony default export */ __webpack_exports__["a"] = (Obstacle);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Canvas_js__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Player = function () {
    function Player() {
        _classCallCheck(this, Player);

        this.x = 5;
        this.y = 5;

        this.width = 60;
        this.direction = 1;
        this.speed = 0;
        this.height = 60;
    }

    _createClass(Player, [{
        key: 'move',
        value: function move() {
            if (this.inBounds(this.y, __WEBPACK_IMPORTED_MODULE_0__Canvas_js__["a" /* default */].getCanvas().height)) this.y += this.direction * this.speed;
        }
    }, {
        key: 'inBounds',
        value: function inBounds(y, maxHeight) {
            if (y + this.speed * this.direction < 0) {
                return false;
            } else if (y + this.speed * this.direction + this.height > maxHeight) {
                return false;
            }

            return true;
        }
    }]);

    return Player;
}();

/* harmony default export */ __webpack_exports__["a"] = (Player);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Game_js__ = __webpack_require__(1);


window.onload = function start() {
    var game = new __WEBPACK_IMPORTED_MODULE_0__Game_js__["a" /* default */]();
};

/***/ })
/******/ ]);